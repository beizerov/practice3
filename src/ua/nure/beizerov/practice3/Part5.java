package ua.nure.beizerov.practice3;


/**
 * This class is represent a Part5.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part5 {
	
	public static void main(String[] args) {
		System.out.println(decimal2Roman(RomanDigit.Unit.I.valueOfNumber));
		System.out.println(decimal2Roman(RomanDigit.Unit.II.valueOfNumber));
		System.out.println(decimal2Roman(RomanDigit.Unit.III.valueOfNumber));
		System.out.println(decimal2Roman(RomanDigit.Unit.IV.valueOfNumber));
		System.out.println(decimal2Roman(RomanDigit.Unit.V.valueOfNumber));
		System.out.println(decimal2Roman(RomanDigit.Unit.VI.valueOfNumber));
		System.out.println(decimal2Roman(RomanDigit.Unit.VII.valueOfNumber));
		System.out.println(decimal2Roman(RomanDigit.Unit.VIII.valueOfNumber));
		System.out.println(decimal2Roman(RomanDigit.Unit.IX.valueOfNumber));
		System.out.println(decimal2Roman(RomanDigit.Ten.X.valueOfNumber));
		System.out.println(decimal2Roman(RomanDigit.Ten.L.valueOfNumber));
		System.out.println(decimal2Roman(
				RomanDigit.Hundred.C.valueOfNumber 
				- RomanDigit.Unit.I.valueOfNumber)
		);
		
		System.out.println(roman2Decimal("XCVI"));
	}
	
	
	public static String decimal2Roman(int x) {
		return getRomanNumber(getDigitsOfNumber(x)).toString();
	}
	
	public static int roman2Decimal(String s) {
		return getDecimalNumber(s);
	}
	
	
	private static class RomanDigit {
				
		enum Unit {
			I(1), II(2), III(3), IV(4), V(5), 
			VI(6), VII(7), VIII(8), IX(9);
			
			private final int value;
			private final int valueOfNumber;
					
			Unit(int value) {
				this.value = value;
				this.valueOfNumber = value;
			}
			
			private static Unit findDigitOfUnits(int digit) {
				for(Unit u : Unit.values()) {
					if (digit == u.value) {
						return u;
					}
				}
				
				return null;
			}
		}
		
		enum Ten {
			X(1, 10), XX(2, 20), XXX(3, 30), XL(4, 40), L(5, 50), 
			LX(6, 60), LXX(7, 70), LXXX(8, 80), XC(9, 90);
			
			private final int value;
			private final int valueOfNumber;
			
			Ten(int value, int valueOfNumber) {
				this.value = value;
				this.valueOfNumber = valueOfNumber;
				
			}
			
			private static Ten findDigitOfTens(int digit) {
				for(Ten t : Ten.values()) {
					if (digit == t.value) {
						return t;
					}
				}
				
				return null;
			}
		}

		enum Hundred {
			C(1, 100);
			
			private final int value;
			private final int valueOfNumber;
			
			Hundred(int value, int valueOfNumber) {
				this.value = value;
				this.valueOfNumber = valueOfNumber;
			}
			
			private static Hundred findDigitOfHundreds(int digit) {
				for(Hundred h : Hundred.values()) {
					if (digit == h.value) {
						return h;
					}
				}
				
				return null;
			}
		}
	}
	
	
	private static class RomanNumber {
		private RomanDigit.Unit units;
		private RomanDigit.Ten tens;
		private RomanDigit.Hundred hundreds;
		
		
		@Override
		public String toString() {
			return  (
					((hundreds != null) ? hundreds.toString() : "")
					+ ((tens != null) ? tens.toString() : "")
					+ ((units != null) ? units.toString() : "")
			);
		}		
	}
	
	
	enum СountingRule {
		I(5, 10),
		V(0, 0),
		X(50, 100),
		L(0, 0),
		C(0, 0);
		
		private final int rule1;
		private final int rule2;
		
		СountingRule(int rule1, int rule2) {
			this.rule1 = rule1;
			this.rule2 = rule2;
		}
		
		private static boolean isAllowed(char character, int digit) {
			СountingRule cr = СountingRule.valueOf(
					String.valueOf(character)
			);
			
			return (digit == cr.rule1 || digit == cr.rule2) ? true : false;
		}
	}
	
	
	private static RomanNumber getRomanNumber (int[] digits) {
		int numberOfDigits = (digits != null) ? digits.length : 0;
		
		if (numberOfDigits != 0) {
			RomanNumber romanNumber = new RomanNumber();
			
			for (int i = 0; i < numberOfDigits; i++) {
				switch (i) {
					case 0:
						romanNumber.units =	(digits[i] != 0) 
							? RomanDigit.Unit.findDigitOfUnits(digits[i]) 
							: null;
						break;
					case 1:
						romanNumber.tens = (digits[i] != 0) 
							? RomanDigit.Ten.findDigitOfTens(digits[i])
							: null
						;
						break;
					case 2:
						romanNumber.hundreds = (digits[i] != 0) 
							? RomanDigit.Hundred.findDigitOfHundreds(digits[i])
							: null
						;
						break;
					default:
						break;
				}
			}
			
			return romanNumber;
		}
		
		return new RomanNumber();
	}
	
	private static int getDecimalNumber(String s) {
		if (!s.matches("^((?=[CLXVI])|C)(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})$")) {
			return 0;
		}
		
		int decimalNumber = 0;
		int rule = 1;
		char[] charsRepresentingNumber = s.toCharArray();

		for (int i = 0; i < charsRepresentingNumber.length; i++) {
			
			if (
					i < charsRepresentingNumber.length - 1
				&& 
					СountingRule.isAllowed(
					charsRepresentingNumber[i], 
					getDecimalDigit(charsRepresentingNumber[i + 1], 1))
			) {
				rule = -1;
			}
			
			decimalNumber += getDecimalDigit(charsRepresentingNumber[i], rule);
			
			rule = 1;
		}
		
		return decimalNumber;
	}
	
	private static int[] getDigitsOfNumber(int number) {
		String strNumber = String.valueOf(number);
		
		int[] digits = new int[strNumber.length()];
		
		for (int i = 0, fromTheEnd = digits.length - 1
			; fromTheEnd > -1
			; i++, fromTheEnd--
		) {
			digits[i] = Integer.parseInt(
					String.valueOf(strNumber.charAt(fromTheEnd))
			);
		}
		
		return digits;
	}	
	
	private static int getDecimalDigit(char romanDigit, int rule) {
		int value = -1;
		String strRepresentationOfDigit = String.valueOf(romanDigit);
		
		СountingRule 
			countingRule = СountingRule.valueOf(strRepresentationOfDigit);
		
		switch(countingRule) {
		case I:
			value = RomanDigit.Unit.valueOf(
					strRepresentationOfDigit
				).valueOfNumber * rule;
				break;
		case V:
			value = RomanDigit.Unit.valueOf(
				strRepresentationOfDigit
			).valueOfNumber * rule;
			break;
		case X:
			value = RomanDigit.Ten.valueOf(
					strRepresentationOfDigit
				).valueOfNumber * rule;
			break;
		case L:
			value = RomanDigit.Ten.valueOf(
					strRepresentationOfDigit
				).valueOfNumber * rule;
			break;
		case C:
			value = RomanDigit.Hundred.valueOf(
					strRepresentationOfDigit
				).valueOfNumber * rule;
			break;
		default:
			break;
		
		}
		
		return value;
	}
}
