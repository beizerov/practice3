package ua.nure.beizerov.practice3;


import static ua.nure.beizerov.practice3.Util.getWordLengthPattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class is represent a Part2.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part2 {

	public static void main(String[] args) {		
		String input = Util.readFile("part2.txt");
		
		System.out.println(convert(input));
	}
	
	
	public static String convert(String input) {
		int maxWordLength = Util.maxWordLength(input);
		int minWordLength = Util.minWordLength(input);
		
		String patternForMinWordLength = getWordLengthPattern(minWordLength);
		String patternForMaxWordLength  = getWordLengthPattern(maxWordLength);
		
		Pattern pattern = Pattern.compile(patternForMinWordLength);
		Matcher matcher = pattern.matcher(input);
		
		StringBuilder output = new StringBuilder();
		
		output
			.append("Min: ")
			.append(
				Util.removeDuplicate(
					Util.getStringWithWordsByLengthOfWord(
							matcher, minWordLength
					)
				).replaceAll(" ", ", ")
			);
		
		pattern = Pattern.compile(patternForMaxWordLength);
		matcher = pattern.matcher(input);
		
		output
			.append("\nMax: ")
			.append(
				Util.removeDuplicate(
					Util.getStringWithWordsByLengthOfWord(
							matcher, maxWordLength
					)
				).replaceAll(" ", ", ")
			);
		
		return output.toString();
	}
}
