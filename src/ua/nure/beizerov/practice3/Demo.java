package ua.nure.beizerov.practice3;


import java.security.NoSuchAlgorithmException;


/**
 * This class is represent a demo.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Demo {

	public static void main(String[] args) {
		Part1.main(null);
		Part2.main(null);
		
		System.out.println();
		
		Part3.main(null);
		
		try {
			Part4.main(new String[] {"asdf", "SHA-256"});
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		System.out.println();
		
		Part5.main(null);
		
		System.out.println();
		
		Part6.main(null);
	}
}
