package ua.nure.beizerov.practice3;


import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class is represent a Part3.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part3 {

	public static void main(String[] args) {
		String input = Util.readFile("part3.txt");

		System.out.println(convert(input));
	}

	
	public static String convert(String input) {
		Pattern pattern = Pattern.compile("(\\b\\p{L})(\\p{L}{2,}\\b)");
		Matcher matcher = pattern.matcher(input);

		StringBuffer sb = new StringBuffer();
		char firstLetter;

		while (matcher.find()) {
			firstLetter = matcher.group(1).charAt(0);
			if (Character.isUpperCase(firstLetter)) {
				matcher.appendReplacement(
						sb, Character.toLowerCase(firstLetter) + "$2"
				);
			} else {
				matcher.appendReplacement(
						sb, Character.toUpperCase(firstLetter) + "$2"
				);
			}
		}

		matcher.appendTail(sb);

		return sb.toString();
	}
}
