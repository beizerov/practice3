package ua.nure.beizerov.practice3;


import java.security.SecureRandom;


/**
 * This class is represent a Part1.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part1 {
	private static final 
		String REG_EXR_FOR_DELETE_FIRST_STRING = "^.*" + System.lineSeparator();
	private static final int SEED_FOR_RANDOM = 10000;
	
	public static void main(String[] args) {
		String input = Util.readFile("part1.txt");
		
		System.out.println(convert1(input));
		System.out.println(convert2(input));
		System.out.println(convert3(input));
		System.out.println(convert4(input));
	}
	
	
	public static String convert1(String input) {
	    return input.replaceAll(REG_EXR_FOR_DELETE_FIRST_STRING, "")
	    		.replaceAll(";.*;", ": ");
	}
	
	public static String convert2(String input) {
	    return input
	    		.replaceAll(REG_EXR_FOR_DELETE_FIRST_STRING, "")
	    		.replaceAll(
	    			"(?m)"
	    			+ "(^\\b\\p{L}+\\b;)"
	    			+ "(\\b\\p{L}+\\b)"
	    			+ "(\\s)"
	    			+ "(\\b\\p{L}+\\b)"
	    			+ "(;)"
	    			+ "(\\b\\p{L}+\\b@\\b\\p{L}+\\b\\.\\b\\p{L}+\\b)"
	    			
	    			, "$4$3$2 (email: $6)");
	}
	
	public static String convert3(String input) {
		input = input
	    		.replaceAll(REG_EXR_FOR_DELETE_FIRST_STRING, "")
	    		.replaceAll(
	    				"(?m)(^\\b\\p{L}+\\b).*@(\\p{L}+.\\p{L}+$)"
	    				
	    				, "$2 $1"
	    		)
	    ;
		
		String[] uniqueEmailNames = Util.getWordsWithoutCopies(
				input
					.replaceAll("(?m)\\s\\b\\p{L}+\\b$", "")
					.split(System.lineSeparator())
		);
		
		String[] data = input.split("\\s");
		
		StringBuilder output = new StringBuilder();
		
		for (int i = 0; i < uniqueEmailNames.length; i++) {
			output
				.append(uniqueEmailNames[i])
				.append(" ")
			;
			
			for (int j = 0; j < data.length; j++) {
				if (uniqueEmailNames[i].matches(data[j])) {
					output
						.append(data[j + 1])
						.append(", ")
					;
				}
			}
			
			output.append(System.lineSeparator());
		}
		
		return output.toString()
				.replaceAll("(?m), $", "")
				.replaceAll("(?m)(?<=\\p{L}) (?=\\p{L})", " ==> ")
		;
	}
	
	public static String convert4(String input) {
		StringBuilder output = new StringBuilder();
		
		String[] outputData =
				
			input
	    		.replaceAll(
		    		"(?m)(^\\b\\p{L}+\\b.\\b\\p{L}+\\b.\\b\\p{L}+\\b$)"
	    			, "$1;Password"
	    		 )
	    		.split(System.lineSeparator())
	    	;
		
		output
			.append(outputData[0])
			.append(System.lineSeparator());
		
		for (int i = 1; i < outputData.length; i++) {
			output
				.append(outputData[i])
				.append(';')
				.append(String.format(
						"%04d%n", new SecureRandom().nextInt(SEED_FOR_RANDOM)
				))
			;
		}
		
	    return output.toString();
	}
}
