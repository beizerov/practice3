package ua.nure.beizerov.practice3;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class is represent a Utility.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public final class Util {

    private static final String ENCODING = "Cp1251";
    
    
    private Util() {
        throw new IllegalStateException("Util class");
      }
    
    
    public static String readFile(String path) {
        String fileResource = null;
        
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(path));
            fileResource = new String(bytes, ENCODING);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        return fileResource;
    }
    
    private static int countUniqueWords(String[] words) {
    	String[] temporaryArray = new String[words.length];
		int numberOfUniqueWords = 1;
		boolean isUnique = false;
		
		for (int i = 1; i < words.length; i++) {
			isUnique = true;
			
			for (int j = 0; j < temporaryArray.length; j++) {
				if (words[i].equals(temporaryArray[j])) {
					isUnique = false;
					break;
				}
			}
			
			if (isUnique) {
				numberOfUniqueWords++;
			}
		}

		return numberOfUniqueWords;
	}
	
	public static String[] getWordsWithoutCopies(String[] words) {
		String[] uniqueWords = (words.length != 0)
								? new String[countUniqueWords(words)]
								: null;
		
		if (uniqueWords == null) {
			return new String[0];
		}						
		
		uniqueWords[0] = words[0];
		
		int index = 1;
		boolean isUnique = false;
		
		for (int i = 1; i < words.length; i++) {
			
			isUnique = true;
			
			for (int j = 0; j < uniqueWords.length; j++) {
				if (words[i].equals(uniqueWords[j])) {
					isUnique = false;
					break;
				}
			}
			
			if (isUnique && index < uniqueWords.length) {
				uniqueWords[index++] = words[i];
			}
		}
		
		
		int uniqueQuantity = 0;
		
		for (String string : uniqueWords) {
			if(string != null) {
				uniqueQuantity++;
			}
		}
		
		String[] temporaryArray = uniqueWords;
		uniqueWords = new String[uniqueQuantity];
		
		for (int i = 0; i < temporaryArray.length; i++) {
			if (temporaryArray[i] != null) {
				uniqueWords[i] = temporaryArray[i];
			}
		}
													
		return uniqueWords;
	}
	
	public static int minWordLength(String input) {
		Pattern pattern = Pattern.compile("\\b\\p{L}+\\b");
		Matcher matcher = pattern.matcher(input);
		
		int minLength = (matcher.find()) ? matcher.group().length() : 0;
		
		while (matcher.find()) {
			final int length = matcher.group().length();

			if (minLength > length) {
				minLength = length;
			}
		}
		
		return minLength;
	}
	
	public static int maxWordLength(String input) {
		Pattern pattern = Pattern.compile("\\b\\p{L}+\\b");
		Matcher matcher = pattern.matcher(input);
		
		int maxLength = 0;
		
		while (matcher.find()) {
			final int length = matcher.group().length();
			
			if (maxLength < length) {
				maxLength = length;
			}
		}
		
		return maxLength;
	}
	
	public static String getWordLengthPattern(int wordLength) {
		return String.format("\\b\\p{L}{%d}\\b", wordLength);
	}
	
	public static String getStringWithWordsByLengthOfWord(
			Matcher matcher, int wordLength
	) {
		StringBuilder data = new StringBuilder();
		
		String word;
		
		while (matcher.find()) {
			word = matcher.group();
			if(word.length() == wordLength) {
				data.append(word).append(" ");
			}
		}
		
		return data.toString();
	}
	
	public static String removeDuplicate(String input) {
		String[] withoutDuplicat = getWordsWithoutCopies(input.split("\\s"));
		
		StringBuilder output = new StringBuilder();
		
		for (String string : withoutDuplicat) {
			output.append(string).append(" ");
		}
		
		return output.toString().trim();
	}
}
