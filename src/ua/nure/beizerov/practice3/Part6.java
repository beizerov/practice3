package ua.nure.beizerov.practice3;


import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class is represent a Part5.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part6 {

	public static void main(String[] args) {
		String input = Util.readFile("part6.txt");
		
		System.out.println(convert(input));
	}
	
	
	public static String convert(String input) {
		String[] words = input.split("\\s");
		
		Pattern pattern = Pattern.compile("(\\b\\p{L}+\\b)");
		Matcher matcher = pattern.matcher(input);

		StringBuffer sb = new StringBuffer();
		String word;
		
		boolean isDuplicate;
		int quantity;
		
		while (matcher.find()) {
			word = matcher.group();
			
			isDuplicate = false;
			quantity = 0;
			
			for (String w : words) {
				if (word.equals(w)) {
					quantity++;
					if(quantity == 2) {
						isDuplicate = true;
						break;
					}
				}
			}
			
			if(isDuplicate) {
				matcher.appendReplacement(sb,  "_$1");
			} else {
				matcher.appendReplacement(sb,  "$1");
			}
		}

		matcher.appendTail(sb);

		return sb.toString();
	}
}
