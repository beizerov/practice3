package ua.nure.beizerov.practice3;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * This class is represent a Part4.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part4 {
	
	public static void main(String[] args) throws NoSuchAlgorithmException {
		String input = (args != null && args.length == 2) ? args[0] : "adf";	
		String algorithm = (args != null && args.length == 2) ? args[1] : "MD5";	
		
		System.out.println(hash(input, algorithm));
	}
	
	
	public static String hash(String input, String algorithm) 
			throws NoSuchAlgorithmException {
		
		MessageDigest digest = MessageDigest.getInstance(algorithm);
		digest.update(input.getBytes());
		byte[] inBytesRepresentation = digest.digest();
		
		StringBuilder hash = new StringBuilder();
		
		for (int i = 0, length = inBytesRepresentation.length
			; i < length
			; i++
		) {
			hash.append(String.format("%02X", inBytesRepresentation[i]));
		}
		
		return hash.toString();
    }
}
